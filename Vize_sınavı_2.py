#!/usr/bin/env python
# coding: utf-8

# In[162]:


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
from sklearn.metrics import r2_score,mean_absolute_error,mean_squared_error
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
df=pd.read_csv("survey lung cancer.csv")
df.head()


# In[163]:


le_GENDER = LabelEncoder()
df.head()


# In[164]:


df['Gender']=le_GENDER.fit_transform(df['GENDER'])
x1=df.drop(['GENDER'],axis='columns')
x1.head()


# In[165]:


Y=x1[x1.LUNGCANCER=="YES"]
N=x1[x1.LUNGCANCER=="NO"]


# In[166]:


plt.scatter(Y.AGE,Y.AGE,color="red",label="Age")
plt.scatter(N.SMOKING,N.SMOKING,color="green",label="smoking")
plt.xlabel("Ages")
plt.ylabel("smokers")
plt.legend()
plt.show()
x1.head()


# In[167]:


x1.LUNGCANCER=[1 if i=="YES" else 0 for i in x1.LUNGCANCER]
##print(df.head())
x1.head()


# In[168]:


y=x1.LUNGCANCER.values
X2=x1.drop(["LUNGCANCER",],axis=1)
X2.head()


# In[169]:


x0=X2
x0=(X2-np.min(X2))/(np.max(X2)-np.min(X2))
x0.head()


# In[170]:


from sklearn.model_selection import train_test_split
x0_train, x0_test,y_train, y_test=train_test_split(x0,y,test_size=0.2,random_state=1)


# In[171]:


## Random forest
from sklearn.ensemble import RandomForestClassifier
RF=RandomForestClassifier(n_estimators=150, random_state=0)
RF.fit(x0_train,y_train)

print("RF Resulet",RF.score(x0_test,y_test))


# In[172]:


##Decision tree
from sklearn.tree import DecisionTreeClassifier

dt=DecisionTreeClassifier(random_state=0)
dt.fit(x0_train,y_train)

print("DT Resulet:",dt.score(x0_test,y_test))


# In[173]:


#KNN
from sklearn.neighbors import KNeighborsClassifier
knn=KNeighborsClassifier(n_neighbors=11)
knn.fit(x0_train,y_train)
knn_pred=knn.predict(x0_test)
knn_score=knn.score(x0_test,y_test)
print(knn_score)


# In[174]:


##SVC
from sklearn.svm import SVC
SVM=SVC(random_state=1)
SVM.fit(x0_train,y_train)

print("SVM Resulet:",svm.score(x0_test,y_test))


# In[ ]:




