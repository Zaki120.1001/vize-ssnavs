#!/usr/bin/env python
# coding: utf-8

# In[158]:


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
from sklearn.metrics import r2_score,mean_absolute_error,mean_squared_error
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
df=pd.read_csv("survey lung cancer1.csv")
df.head()


# In[159]:


inputs= df.drop('LUNG_CANCER', axis='columns')
target= df['LUNG_CANCER']
inputs.head()


# In[160]:


le_GENDER = LabelEncoder()


# In[161]:


inputs['Gender']=le_GENDER.fit_transform(inputs['GENDER'])
inputs_1=inputs.drop(['GENDER'],axis='columns')
inputs_1.head()


# In[162]:


##Decision Tree
##Prediction
test_1=DecisionTreeRegressor(random_state=0)
test_1.fit(inputs_1,target)
Prediction=test_1.predict([(74,1,2,2,1,1,2,1,2,2,2,2,1,1,0)]) 
Prediction
print(Prediction)
print(test_1.score(inputs_1,target))


# In[163]:


##Multiple linear regression
Mlr=LinearRegression()
Mlr.fit(inputs_1,target)
print(Mlr.intercept_,mlr.coef_)
MLT_pred=test_1.predict([[71,1,2,2,1,1,1,1,2,1,2,1,2,1,1]])
MLr_pred=test_1.predict(inputs_1)

print(int(MLT_pred))
print(Mlr.score(inputs_1,target))
print("MLR R2 Score:",r2_score(target,MLr_pred))
print("MLR Mean Absolute Error:",mean_absolute_error(target,MLr_pred))
print("MLR  Mean Squared Error:",mean_squared_error(target,MLr_pred))
print("MLR Mean Squared Error:",(mean_squared_error(target,MLr_pred)**0.5))


# In[164]:


##Random Fotest
from sklearn.ensemble import RandomForestRegressor
RF=RandomForestRegressor(n_estimators=90)
RF.fit(inputs_1,target)
RF.score(inputs_1,target)

